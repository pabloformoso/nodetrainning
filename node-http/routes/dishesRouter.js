var express = require('express');
var router = express.Router();

// Default router
router.route("/").all(function(req, res, next) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  next();  
}).get(function(req, res, next) {
  res.end('Will send disehes!');
}).post(function(req, res, next) {
  res.end('Will POST disehes!');
}).delete(function(req, res, next) {
  res.end('Delete all dishes!');
});

// Default router
router.route("/:dishId").all(function(req, res, next) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  next();  
}).get(function(req, res, next) {
  res.end('Will send the dish with id:' + req.params.dishId)
}).post(function(req, res, next) {
  res.end('Will post the dish with id:' + req.params.dishId);
}).delete(function(req, res, next) {
  res.end('Will delete  the dish with id:' + req.params.dishId);
});

module.exports = router;
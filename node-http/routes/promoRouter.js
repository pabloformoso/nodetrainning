var express = require('express');
var router = express.Router();

// Default router
router.route("/").all(function(req, res, next) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  next();  
}).get(function(req, res, next) {
  res.end('Will send promotions!');
}).post(function(req, res, next) {
  res.end('Will POST promotions!');
}).delete(function(req, res, next) {
  res.end('Delete all promotions!');
});

// Default router
router.route("/:promoId").all(function(req, res, next) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  next();  
}).get(function(req, res, next) {
  res.end('Will send promo with id:' + req.params.promoId)
}).post(function(req, res, next) {
  res.end('Will post promo with id:' + req.params.promoId);
}).delete(function(req, res, next) {
  res.end('Will delete  promo with id:' + req.params.promoId);
});

module.exports = router;
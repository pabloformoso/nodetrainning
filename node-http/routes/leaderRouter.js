var express = require('express');
var router = express.Router();

// Default router
router.route("/").all(function(req, res, next) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  next();  
}).get(function(req, res, next) {
  res.end('Will send leaderships!');
}).post(function(req, res, next) {
  res.end('Will POST leaderships!');
}).delete(function(req, res, next) {
  res.end('Delete all leaderships!');
});

// Default router
router.route("/:leaderId").all(function(req, res, next) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  next();  
}).get(function(req, res, next) {
  res.end('Will send leadership with id:' + req.params.leaderId)
}).post(function(req, res, next) {
  res.end('Will post leadership with id:' + req.params.leaderId);
}).delete(function(req, res, next) {
  res.end('Will delete leadership with id:' + req.params.leaderId);
});

module.exports = router;
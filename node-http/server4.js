var express = require("express");
var morgan = require("morgan");
var bodyParser = require("body-parser");


var hostname = "localhost";
var port = 3000;

var app = express();
app.use(morgan('dev'));

var router = express.Router();
router.use(bodyParser.json());

var dishRouter = require("./routes/dishesRouter");
var leaderRouter = require("./routes/leaderRouter");
var promoRouter = require("./routes/promoRouter");
app.use('/dishes', dishRouter);
app.use('/promotions', promoRouter);
app.use('/leadership', leaderRouter);

app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function () {
  console.log("Server Running");
}); 
var express = require("express");
var morgan = require("morgan");
var bodyParser = require("body-parser");


var hostname = "localhost";
var port = 3000;

var app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());

app.all('/dishes', function(req, res, next) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  next();
});

app.get('/dishes', function(req, res, next) {
  res.end('Will send disehes!');
});

app.get('/dishes/:dishId', function(req, res, next) {
  res.end('Will send the dish with id:' + req.params.dishId);
});

app.put('/dishes/:dishId', function(req, res, next) {
  res.end('Will update the dish with id:' + req.params.dishId);
});

app.delete('/dishes/:dishId', function(req, res, next) {
  res.end('Will delete the dish with id:' + req.params.dishId);
});

app.post('/dishes', function(req, res, next) {
  res.end('Will POST disehes!');
});

app.delete('/dishes', function(req, res, next) {
  res.end('Delete all dishes!');
});


app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function () {
  console.log("Server Running");
}); 
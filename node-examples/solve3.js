var argv = require("yargs")
  .usage("Use node $0 --l=[num] --b=[num]")
  .demand(['l', 'b'])
  .argv;
  
var rect = require("./rectangle2");
  
 /*
  *
  */
function solveRect(l,b) {
  console.log("Solving for rectangle with l = " + l + " and b = " + b);
  
  rect(l,b, function (error, rectangle) {
    if (error) {
      console.log(error);
    } else {
      console.log("Area: "      + rectangle.area());
      console.log("Perimeter: " + rectangle.perimeter());
    }
  });
};

solveRect(argv.l, argv.b);
solveRect(3,5);
solveRect(-3, 5);